/**
 * Autoři: Jan Herec - xherec00, Pavel Juhaňák - xjuhan01
 * Kódování: UTF-8 (bez BOM)
 * Datum vytvořeni: 24. 4. 2015
 * Datum poslední revize: 25. 4. 2015
 */

/**
 * Smazáni tabulek a zároveň potlačení výjimek při mazáni neexistujicích tabulek při prvním běhu skriptu
 */
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE pozice CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE zamestnanec CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE majitel CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE zvire CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE lecba CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE druh CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE lek CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE nemoc CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE lek_nemoc CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE druh_lek CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP TABLE lecba_lek CASCADE CONSTRAINTS';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP SEQUENCE id_zvirete_seq';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/
BEGIN
  EXECUTE IMMEDIATE 'DROP MATERIALIZED VIEW honzuv_pohled';
EXCEPTION
  WHEN OTHERS THEN NULL;
END;
/

/**
 * Vytvoření tabulek a dalších databázových objektů jako sekvence
 */
CREATE TABLE pozice(
  ID_pozice INTEGER PRIMARY KEY,
  nazev NVARCHAR2(50) NOT NULL,
  hodinova_mzda NUMBER(6)
);

CREATE TABLE zamestnanec(
  ID_zamestnance INTEGER PRIMARY KEY,
  ID_pozice INTEGER NOT NULL,  
  jmeno NVARCHAR2(50) NOT NULL,
  prijmeni NVARCHAR2(50) NOT NULL,
  titul NVARCHAR2(20),
  ulice NVARCHAR2(50),
  mesto NVARCHAR2(50),
  psc NUMBER(5),
  cislo_uctu NUMBER(14)
);

CREATE TABLE majitel(
  ID_majitele INTEGER PRIMARY KEY,
  jmeno NVARCHAR2(50) NOT NULL,
  prijmeni NVARCHAR2(50) NOT NULL,
  ulice NVARCHAR2(50),
  mesto NVARCHAR2(50),
  psc NUMBER(5),
  cislo_uctu NUMBER(14),
  typ NVARCHAR2(20),
  rodne_cislo NUMBER(10),
  cislo_OP NUMBER(9),
  ICO NUMBER(8),
  DIC NUMBER(12)
);

CREATE TABLE zvire(
  ID_zvirete INTEGER PRIMARY KEY,
  ID_majitele INTEGER NOT NULL,
  ID_druhu INTEGER NOT NULL,
  jmeno NVARCHAR2(50) NOT NULL,
  datum_narozeni DATE,
  pohlavi NVARCHAR2(10) NOT NULL,
  vaha NUMBER(3),
  barva NVARCHAR2(50),
  dat_posl_prohl DATE
);

CREATE TABLE druh(
  ID_druhu INTEGER PRIMARY KEY,
  nazev NVARCHAR2(50) NOT NULL
);

CREATE TABLE lek(
  ID_leku INTEGER PRIMARY KEY,
  nazev NVARCHAR2(50) NOT NULL,
  typ NVARCHAR2(30),
  ucinna_latka NVARCHAR2(50),
  kontraindikace NVARCHAR2(500)
);

CREATE TABLE druh_lek(
  ID_druhu INTEGER NOT NULL,
  ID_leku INTEGER NOT NULL, 
  doporucene_davkovani NVARCHAR2(100),
  vedlejsi_ucinky NVARCHAR2(100)
);

CREATE TABLE nemoc(
  ID_nemoci INTEGER PRIMARY KEY,
  nazev NVARCHAR2(50) NOT NULL,
  priznaky NVARCHAR2(200)
);

CREATE TABLE lek_nemoc(
  ID_leku INTEGER NOT NULL,
  ID_nemoci INTEGER NOT NULL, 
  delka_lecby NVARCHAR2(30),
  procento_uspecne_lecby NUMBER(5,2)
);

CREATE TABLE lecba_lek(
  ID_zamestnance INTEGER NOT NULL, 
  poradove_cislo_lecby INTEGER NOT NULL, 
  ID_zvirete INTEGER NOT NULL, 
  ID_leku INTEGER NOT NULL,
  datum_nasazeni DATE,
  doba_podavani NVARCHAR2(30),
  davkovani NVARCHAR2(30)
);

CREATE TABLE lecba(
  poradove_cislo_lecby INTEGER NOT NULL,
  ID_zamestnance INTEGER NOT NULL, 
  ID_zvirete INTEGER NOT NULL, 
  ID_nemoci INTEGER NOT NULL,
  upresneni_diagnozy NVARCHAR2(100),
  datum_zahajeni_lecby DATE,
  stav NVARCHAR2(50) NOT NULL,
  cena NUMBER(8,2)
);

CREATE SEQUENCE id_zvirete_seq; 

/**
 * Integritní omezení typu složeného PRIMARY KEY
 */
ALTER TABLE lecba ADD CONSTRAINT PK_lecba PRIMARY KEY (poradove_cislo_lecby, ID_zvirete);
ALTER TABLE druh_lek ADD CONSTRAINT PK_druh_lek PRIMARY KEY (ID_druhu, ID_leku);
ALTER TABLE lek_nemoc ADD CONSTRAINT PK_lek_nemoc PRIMARY KEY (ID_leku, ID_nemoci);
ALTER TABLE lecba_lek ADD CONSTRAINT PK_lecba_lek PRIMARY KEY (poradove_cislo_lecby, ID_zvirete, ID_leku);

/**
 * Integritní omezení typu FOREIGN KEY
 */
ALTER TABLE zamestnanec ADD CONSTRAINT fk_zam_poz FOREIGN KEY (ID_pozice) REFERENCES pozice(ID_pozice) ON DELETE CASCADE;
ALTER TABLE zvire ADD CONSTRAINT fk_zvi_maj FOREIGN KEY (ID_majitele) REFERENCES majitel(ID_majitele) ON DELETE CASCADE;
ALTER TABLE zvire ADD CONSTRAINT fk_zvi_druh FOREIGN KEY (ID_druhu) REFERENCES druh(ID_druhu) ON DELETE CASCADE;
ALTER TABLE druh_lek ADD CONSTRAINT fk_druh_lek_druh FOREIGN KEY (ID_druhu) REFERENCES druh(ID_druhu) ON DELETE CASCADE;
ALTER TABLE druh_lek ADD CONSTRAINT fk_druh_lek_lek FOREIGN KEY (ID_leku) REFERENCES lek(ID_leku) ON DELETE CASCADE;
ALTER TABLE lek_nemoc ADD CONSTRAINT fk_lek_nemoc_lek FOREIGN KEY (ID_leku) REFERENCES lek(ID_leku) ON DELETE CASCADE;
ALTER TABLE lek_nemoc ADD CONSTRAINT fk_lek_nemoc_nemoc FOREIGN KEY (ID_nemoci) REFERENCES nemoc(ID_nemoci) ON DELETE CASCADE;
ALTER TABLE lecba_lek ADD CONSTRAINT fk_lecba_lek_zamestnanec FOREIGN KEY (ID_zamestnance) REFERENCES zamestnanec(ID_zamestnance) ON DELETE CASCADE;
ALTER TABLE lecba_lek ADD CONSTRAINT fk_lecba_lek_lecba FOREIGN KEY (poradove_cislo_lecby, ID_zvirete) REFERENCES lecba(poradove_cislo_lecby, ID_zvirete) ON DELETE CASCADE;
ALTER TABLE lecba_lek ADD CONSTRAINT fk_lecba_lek_lek FOREIGN KEY (ID_leku) REFERENCES lek(ID_leku) ON DELETE CASCADE;
ALTER TABLE lecba ADD CONSTRAINT fk_lecba_zamestnanec FOREIGN KEY (ID_zamestnance) REFERENCES zamestnanec(ID_zamestnance) ON DELETE CASCADE;
ALTER TABLE lecba ADD CONSTRAINT fk_lecba_zvire FOREIGN KEY (ID_zvirete) REFERENCES zvire(ID_zvirete) ON DELETE CASCADE;
ALTER TABLE lecba ADD CONSTRAINT fk_lecba_nemoc FOREIGN KEY (ID_nemoci) REFERENCES nemoc(ID_nemoci) ON DELETE CASCADE;

/**
 * Integritní omezení typu CHECK
 */
ALTER TABLE majitel ADD CONSTRAINT enum_majitel_typ CHECK ( typ IN ('fyzická osoba','právnická osoba') );
ALTER TABLE majitel ADD CONSTRAINT check_rodne_cislo CHECK ( MOD(rodne_cislo, 11) = 0 );
ALTER TABLE zvire ADD CONSTRAINT enum_zvire_pohlavi CHECK ( pohlavi IN ('samec','samice') );
ALTER TABLE lek ADD CONSTRAINT enum_lek_typ CHECK ( typ IN ('mast','tablety','kapky','čípek') );
ALTER TABLE lecba ADD CONSTRAINT enum_lecba_stav CHECK ( stav IN ('započatá','dokončená') );

/**
 * Naplnění tabulek ukázkovými daty
 */
INSERT INTO pozice VALUES(1,'Doktor',40000);
INSERT INTO pozice VALUES(2,'Sestra',20000);

INSERT INTO zamestnanec VALUES(1,1,'Martin','Beránek','Dr.','Štefánikova 722','Vyškov',68201,47163483324813);
INSERT INTO zamestnanec VALUES(2,2,'Radmila','Grundzová','Bc.','Masarykova 923','Humpolec',39601,55097551119036);
INSERT INTO zamestnanec VALUES(3,1,'Ludmila','Janoušková','Dr.','Černá 22','Okřice',68201,47176483324813);
INSERT INTO zamestnanec VALUES(4,2,'Lukáš','Malý','Bc.','Římská 923','Slavičín',39601,55098751119036);

INSERT INTO majitel (ID_majitele, jmeno, prijmeni, ulice, mesto, psc)
  VALUES(1,'Pavel','Juhaňák','Sportovní 407','Hanušovice',78833);
INSERT INTO majitel (ID_majitele, jmeno, prijmeni, ulice, mesto, psc)
  VALUES(2,'Stanislava','Pilařová','Fugnerova 816','Knežmost',29402);
INSERT INTO majitel (ID_majitele, jmeno, prijmeni, ulice, mesto, psc)
  VALUES(3,'Václav','Toušek','Rvačov 358','Roudnice nad Labem ',41301);

INSERT INTO druh VALUES(1,'pes');
INSERT INTO druh VALUES(2,'kočka');
INSERT INTO druh VALUES(3,'želva');

INSERT INTO zvire (ID_zvirete, ID_majitele, ID_druhu, jmeno, datum_narozeni, pohlavi, barva)
  VALUES(id_zvirete_seq.nextval,1,1,'Amor',TO_DATE('20.03.2002', 'dd.mm.yyyy'),'samec','zlatá');
INSERT INTO zvire (ID_zvirete, ID_majitele, ID_druhu, jmeno, datum_narozeni, pohlavi, barva)
  VALUES(id_zvirete_seq.nextval,2,2,'Micka',TO_DATE('15.08.2013', 'dd.mm.yyyy'),'samice','hnědá');
INSERT INTO zvire (ID_zvirete, ID_majitele, ID_druhu, jmeno, datum_narozeni, pohlavi, barva)
  VALUES(id_zvirete_seq.nextval, 1,3,'Žofka', TO_DATE('16.08.2013', 'dd.mm.yyyy'), 'samice','zeleno - žlutá');
INSERT INTO zvire (ID_zvirete, ID_majitele, ID_druhu, jmeno, datum_narozeni, pohlavi)
  VALUES(id_zvirete_seq.nextval, 3,1,'Guláš', TO_DATE('16.09.2013', 'dd.mm.yyyy'),'samec');

INSERT INTO lek (ID_leku, nazev, typ)
  VALUES(1,'Endiaron','tablety');
INSERT INTO lek (ID_leku, nazev, typ)
  VALUES(2,'ALAVIS','mast');
INSERT INTO lek (ID_leku, nazev, typ, ucinna_latka)
  VALUES(3,'FAKTU','mast', 'policresulen,cinchocain');
INSERT INTO lek (ID_leku, nazev, typ, ucinna_latka)
  VALUES(4,'PROCTO-GLYVENOL', 'mast', 'tribenosid,lidokain'); 
INSERT INTO lek (ID_leku, nazev, typ, ucinna_latka)
  VALUES(5,'SPOFAX', 'čípek', 'cinchokain');  

INSERT INTO druh_lek (ID_druhu, ID_leku, doporucene_davkovani)
  VALUES(1,1,'jednu tabletu denně, při velkých obtížích dvě');
INSERT INTO druh_lek (ID_druhu, ID_leku, doporucene_davkovani)
  VALUES(2,1,'půl tablety denně, při velkých obtížích jednu');
INSERT INTO druh_lek (ID_druhu, ID_leku, doporucene_davkovani)
  VALUES(1,2,'nanášet pravidelně po třech hodinách (v noci není potřeba)');

INSERT INTO nemoc (ID_nemoci, nazev, priznaky)
  VALUES(1, 'průjmové onemocnění', 'řídká stolice, malátnost');
INSERT INTO nemoc (ID_nemoci, nazev)
  VALUES(2, 'poranění kůže');
INSERT INTO nemoc (ID_nemoci, nazev, priznaky)
  VALUES(3, 'hemeroidy', 'krvácení konečníku');

INSERT INTO lek_nemoc (ID_leku, ID_nemoci, delka_lecby, procento_uspecne_lecby)
  VALUES(1, 1, 'dva týdny', 95.50); 
INSERT INTO lek_nemoc (ID_leku, ID_nemoci, delka_lecby)
  VALUES(2, 2, 'týden až dva');   
INSERT INTO lek_nemoc (ID_leku, ID_nemoci, delka_lecby)
  VALUES(3, 3, 'měsíc až dva, někdy celý život');  
INSERT INTO lek_nemoc (ID_leku, ID_nemoci, delka_lecby)
  VALUES(4, 3, 'měsíc až dva, někdy celý život');  
INSERT INTO lek_nemoc (ID_leku, ID_nemoci, delka_lecby)
  VALUES(5, 3, 'měsíc až dva, někdy celý život');  
  
INSERT INTO lecba (poradove_cislo_lecby, ID_zamestnance, ID_zvirete, ID_nemoci, datum_zahajeni_lecby, stav, cena)
  VALUES(1, 1, 1, 1, TO_DATE('10.10.2015', 'dd.mm.yyyy'), 'započatá', 30000.00); 
INSERT INTO lecba (poradove_cislo_lecby, ID_zamestnance, ID_zvirete, ID_nemoci, upresneni_diagnozy, datum_zahajeni_lecby, stav, cena)
  VALUES(2, 1, 1, 2, 'jemně krvácivá rána, drobné poranění škáry, ', TO_DATE('20.10.2015', 'dd.mm.yyyy'), 'dokončená', 2000.00); 
INSERT INTO lecba (poradove_cislo_lecby, ID_zamestnance, ID_zvirete, ID_nemoci, datum_zahajeni_lecby, stav, cena)
  VALUES(1, 2, 2, 1, TO_DATE('10.11.2015', 'dd.mm.yyyy'), 'započatá', 30000.00); 
INSERT INTO lecba (poradove_cislo_lecby, ID_zamestnance, ID_zvirete, ID_nemoci, datum_zahajeni_lecby, stav, cena)
  VALUES(2, 2, 2, 3, TO_DATE('20.12.2015', 'dd.mm.yyyy'), 'dokončená', 10000.00); 
INSERT INTO lecba (poradove_cislo_lecby, ID_zamestnance, ID_zvirete, ID_nemoci, datum_zahajeni_lecby, stav, cena)
  VALUES(1, 3, 3, 3, TO_DATE('25.12.2015', 'dd.mm.yyyy'), 'započatá', 500.00); 
INSERT INTO lecba (poradove_cislo_lecby, ID_zamestnance, ID_zvirete, ID_nemoci, datum_zahajeni_lecby, stav, cena)
  VALUES(2, 2, 3, 3, TO_DATE('21.11.2015', 'dd.mm.yyyy'), 'započatá', 300.00); 

INSERT INTO lecba_lek (ID_zamestnance, poradove_cislo_lecby, ID_zvirete, ID_leku, datum_nasazeni, doba_podavani, davkovani)
  VALUES(1, 1, 1, 1, TO_DATE('20.09.2015', 'dd.mm.yyyy'), 'dva až tři týdny', '2x denně'); 
INSERT INTO lecba_lek (ID_zamestnance, poradove_cislo_lecby, ID_zvirete, ID_leku, datum_nasazeni, doba_podavani)
  VALUES(1, 2, 1, 2, TO_DATE('20.08.2015', 'dd.mm.yyyy'), 'dva až tři týdny'); 
INSERT INTO lecba_lek (ID_zamestnance, poradove_cislo_lecby, ID_zvirete, ID_leku, datum_nasazeni, doba_podavani, davkovani)
  VALUES(2, 1, 2, 1, TO_DATE('20.08.2015', 'dd.mm.yyyy'), 'měsíc', '3x denně'); 
INSERT INTO lecba_lek (ID_zamestnance, poradove_cislo_lecby, ID_zvirete, ID_leku, datum_nasazeni, doba_podavani)
  VALUES(1, 2, 2, 3, TO_DATE('20.09.2015', 'dd.mm.yyyy'), 'tři týdny'); 
INSERT INTO lecba_lek (ID_zamestnance, poradove_cislo_lecby, ID_zvirete, ID_leku, datum_nasazeni, doba_podavani)
  VALUES(2, 1, 3, 4, TO_DATE('26.09.2015', 'dd.mm.yyyy'), 'alespoň čtyři týdny'); 
      
-- triger který slouží pro automatické generování hodnot primárního klíče tabulky zvire, 
-- pokud nebyl primární klíč zadán v příkazu INSERT      
CREATE OR REPLACE TRIGGER ID_ZVIRE
  before insert on zvire
  for each row  
  begin   
    if :NEW.id_zvirete is null then 
      :NEW.id_zvirete := id_zvirete_seq.nextval;
    end if; 
  end;       
  /         

-- dotazy vkládají nové zvíře, ale bez jeho primárního klíče, o to se právě postará trigger ID_ZVIRE, 
-- který primární klíč automaticky doplní
INSERT INTO zvire (ID_majitele, ID_druhu, jmeno, pohlavi, barva)
  VALUES(1,3,'Míla','samice','zelaná');
INSERT INTO zvire (ID_majitele, ID_druhu, jmeno, pohlavi, barva)
  VALUES(1,1,'Max','samec', 'rezavá');
    
-- triger automaticky nastavi datum zahajeni lecby na aktualni systemové datum
-- pokud datum zahajeni nebylo nastaveno rucne        
CREATE OR REPLACE TRIGGER zahajeni_lecby
  before insert on lecba
  for each row  
  begin   
    if :NEW.datum_zahajeni_lecby is null then 
      :NEW.datum_zahajeni_lecby := SYSDATE;
    end if; 
  end;
  /   
     
-- trigger automaticky aktualizuje datum poslední prohlídky zvířete, pokud dojde k nové léčbě zvířete, nebo k aktualizaci stávající léčby
CREATE OR REPLACE TRIGGER posledni_prohlidka_zvirete
  after insert or update on lecba
  for each row  
  begin   
    UPDATE zvire SET zvire.dat_posl_prohl = SYSDATE WHERE zvire.ID_zvirete = :NEW.id_zvirete;
  end;
  /      

-- dotazy vkládají novou léčbu, ale nespecifikují datum zahájení léčby, o to se právě postará trigger zahajeni_lecby, 
-- který datum zahájení léčby automaticky doplní za základě současného data
INSERT INTO lecba (poradove_cislo_lecby, ID_zamestnance, ID_zvirete, ID_nemoci, stav, cena)
  VALUES(3, 1, 2, 3, 'započatá', 10000.00); 
INSERT INTO lecba (poradove_cislo_lecby, ID_zamestnance, ID_zvirete, ID_nemoci, stav, cena)
  VALUES(1, 2, 5, 1, 'započatá', 500.00);  
        
-- dotazy aktualizují léčbu zvířete, aby se nemuselo manuálně přidávat ke zvířeti datum poslední prohlídky, pokud dojde k úpravě léčby zvířete, tak toto ošetřuje trigger posledni_prohlidka_zvirete

UPDATE lecba SET stav = 'dokončená' WHERE poradove_cislo_lecby = 3 and ID_zvirete = 2;
UPDATE lecba SET stav = 'dokončená' WHERE poradove_cislo_lecby = 1 and ID_zvirete = 5;
      

-- procedura odstraní ty zaměstnance, kteří jsou líní a málo pracují (viz. kurzor cursor_lini_zamestnanci)
-- doporučuje se spouštět tuto proceduru 1x ročně pro odfiltrování všech méně schopných pracovníků, 
-- zůstanou jen ti nejlepší a nejpracovitější.
CREATE OR REPLACE PROCEDURE odstran_line_zamestnance AS
zam_id zamestnanec.ID_zamestnance%type;

-- kurzor, který představuje dotaz, kdy vybereme všechny pracovníky (líné), 
-- kteří provedli méně léčeb než je průměrný počet léčeb na pracovníka 
-- (tento průměrný počet léčeb se počítá jen pro pracovníky, kteří provedli alespoň jednu léčbu, 
-- takže pracovníci, kteří žádnou léčbu neprovedli tento průměr nesnižují).      
CURSOR cursor_lini_zamestnanci IS 
  SELECT DISTINCT z.ID_zamestnance
  FROM zamestnanec z
  WHERE z.ID_zamestnance NOT IN(
    SELECT ID_zamestnance FROM(
      SELECT zamestnanec.ID_zamestnance, COUNT(*) FROM lecba, zamestnanec
        WHERE zamestnanec.ID_zamestnance = lecba.ID_zamestnance
        GROUP BY zamestnanec.ID_zamestnance
        HAVING COUNT(*) >= ALL (
          SELECT AVG(prumer) FROM (
            SELECT zamestnanec.ID_zamestnance, COUNT(*) AS prumer FROM lecba, zamestnanec
            WHERE zamestnanec.ID_zamestnance = lecba.ID_zamestnance GROUP BY zamestnanec.ID_zamestnance))));
BEGIN 
   OPEN cursor_lini_zamestnanci; 
   LOOP 
     FETCH cursor_lini_zamestnanci INTO zam_id; 
     EXIT WHEN cursor_lini_zamestnanci%NOTFOUND; 
     DELETE FROM zamestnanec WHERE zamestnanec.ID_zamestnance = zam_id;
   END LOOP; 
   CLOSE cursor_lini_zamestnanci; 
END;      
/   

-- sputíme proceduru, která odstraní líné zaměstance
BEGIN
  odstran_line_zamestnance();
END;
/       
      
-- procedura dá slevu nejlepšímu zákazníkovi, kdo takový zákazník je si pojďme nyní vysvětlit:
-- je to zákazník, který doposud zaplatil na všech dokončených léčbách nejvíce ze všech zákazníků.
-- sleva se týká léčeb, které pro daného zákazníka aktuálně probíhají (jsou ve stavu započaté)
-- pokud tento nejlepší zákazník žádnou takovou započatou léčbu nemá, tak se ho sleva netýká
-- tato procedura ošetřuje 2 výjimky: 
-- 1) jednak ošetřuje to, jestli je zadáná částka (sleva) číslo a pokud není dojde k výjímce, která je ošetřena chybovým hlášením
-- 2) druhak ošetřuje to, jestli je zadaná částka (sleva) nezáporné číslo 
--(kdyby bylo záporné, tak by se vlastně nejednalo o slevu, ale naopak o navýšení ceny léčby) 
-- a pokud se tedy jedná o záporné číslo, tak je vypsáno chybové hlášení
CREATE OR REPLACE PROCEDURE sleva_pro_nejlepsiho_zakaznika(
  castka_in IN NVARCHAR2
) AS
zaporna_sleva EXCEPTION;
castka number;
BEGIN
    castka := castka_in;
    IF castka < 0 THEN
        RAISE zaporna_sleva;
    END IF;
    UPDATE lecba SET cena = (lecba.cena - castka)
    WHERE lecba.stav = 'započatá' and lecba.ID_zvirete IN 
        (SELECT zvire.ID_zvirete FROM zvire, majitel 
        WHERE zvire.ID_majitele = majitel.ID_majitele AND majitel.ID_majitele IN 
            (SELECT ID_majitele FROM 
            (SELECT majitel.ID_majitele, SUM(cena) FROM majitel, zvire, lecba 
            WHERE majitel.ID_majitele = zvire.ID_majitele AND zvire.ID_zvirete = lecba.ID_zvirete 
            GROUP BY majitel.ID_majitele HAVING SUM(cena) >= ALL 
                (SELECT cena_tmp FROM 
                    (SELECT SUM(cena) AS cena_tmp,majitel.ID_majitele FROM lecba, majitel, zvire 
                    WHERE majitel.ID_majitele = zvire.ID_majitele AND zvire.ID_zvirete = lecba.ID_zvirete 
                    AND lecba.stav = 'dokončená' 
                    GROUP BY majitel.ID_majitele)))));
EXCEPTION
   WHEN zaporna_sleva THEN   
       DBMS_OUTPUT.put_line('Chyba, zadaná částka je záporná!');
   WHEN VALUE_ERROR THEN
       DBMS_OUTPUT.PUT_LINE ('Chyba, zadaná částka není číslo!');                
END;
/
-- sputíme proceduru, která dá slevu nejlepšímu zákazníkovi a to konkrétně 500kč
BEGIN
  sleva_pro_nejlepsiho_zakaznika(500);
END;
/      

-- ukázka ošetření výjimky, v případě že není jako sleva zadáno číslo ale řetězec
BEGIN
  sleva_pro_nejlepsiho_zakaznika('slssl');
END;
/     

-- ukázka ošetření výjimky, v případě že je zadána záporná sleva (takže se jedná o navýšení, nikoliv o slevu, což nechceme)
BEGIN
  sleva_pro_nejlepsiho_zakaznika(-500);
END;
/     
     
-- Vypíšeme explain plan pro dotaz který vybere zvířata, která se narodila v roce 2013 a seskupí je podle celkových nákladů na jejich léčbu a to jen ty zvířata, jejichž cena léčeb je větší než 2000
EXPLAIN PLAN SET
  STATEMENT_ID 'my_explained_query'
FOR
SELECT zvire.ID_zvirete, zvire.jmeno, SUM(cena) celkove_vydaje
  FROM lecba, zvire
  WHERE zvire.ID_zvirete = lecba.ID_zvirete AND datum_narozeni BETWEEN TO_DATE('01.01.2013', 'dd.mm.yyyy') AND TO_DATE('30.12.2013', 'dd.mm.yyyy')
  GROUP BY zvire.ID_zvirete, zvire.jmeno
  HAVING SUM(cena) > 2000;

SELECT plan_table_output
  FROM table (dbms_xplan.display('plan_table','my_explained_query','typical'));
  
-- Vytvoříme index na sloupci datum narození což by mělo další explain plan zrychlit 
CREATE INDEX PIndex
ON zvire (datum_narozeni);  


-- Vypíšeme explain plan pro dotaz který vybere zvířata, která se narodila v roce 2013 a seskupí je podle celkových nákladů na jejich léčbu a to jen ty zvířata, jejichž cena léčeb je větší než 2000, nyní již dopadne cena dotazu lépe neý minule kvůli definovanému indexu, který dotaz zrychlí
EXPLAIN PLAN SET
  STATEMENT_ID 'my_explained_query3'
FOR
SELECT zvire.ID_zvirete, zvire.jmeno, SUM(cena) celkove_vydaje
  FROM lecba, zvire
  WHERE zvire.ID_zvirete = lecba.ID_zvirete AND datum_narozeni BETWEEN TO_DATE('01.01.2013', 'dd.mm.yyyy') AND TO_DATE('30.12.2013', 'dd.mm.yyyy')
  GROUP BY zvire.ID_zvirete, zvire.jmeno
  HAVING SUM(cena) > 2000;

SELECT plan_table_output
  FROM table (dbms_xplan.display('plan_table','my_explained_query3','typical'));

     
-- prideleni opravneni uzivateli xherec00

GRANT ALL ON id_zvirete_seq TO xherec00;

GRANT ALL ON druh TO xherec00;
GRANT ALL ON druh_lek TO xherec00;
GRANT ALL ON lecba TO xherec00;
GRANT ALL ON lecba_lek TO xherec00;
GRANT ALL ON lek TO xherec00;
GRANT ALL ON lek_nemoc TO xherec00;
GRANT ALL ON majitel TO xherec00;
GRANT ALL ON nemoc TO xherec00;
GRANT ALL ON pozice TO xherec00;
GRANT ALL ON zamestnanec TO xherec00;
GRANT ALL ON zvire TO xherec00;

GRANT EXECUTE ON odstran_line_zamestnance TO xherec00;
GRANT EXECUTE ON sleva_pro_nejlepsiho_zakaznika TO xherec00;
/      
      
-- ukázka použití materializovaného pohledu, nyní si necháme vypsat plán na provedení dotazu na zaměstanance a jejich pozice
EXPLAIN PLAN SET
  STATEMENT_ID 'my_explained_query3'
FOR
SELECT Z.jmeno, Z.prijmeni, Z.titul, P.nazev as POZICE
  FROM xjuhan01.pozice P, xjuhan01.zamestnanec Z
  WHERE P.ID_pozice = Z.ID_pozice
  ORDER BY POZICE;

SELECT plan_table_output
  FROM table (dbms_xplan.display('plan_table','my_explained_query3','typical'));
           
-- vytvoříme materializovaný pohle, který zmaterializuje předchozí dotaz do fyzické tabulky
CREATE MATERIALIZED VIEW honzuv_pohled
nologging
cache
build immediate
REFRESH ON COMMIT
enable query rewrite
AS
SELECT Z.jmeno, Z.prijmeni, Z.titul, P.nazev as POZICE
  FROM xjuhan01.pozice P, xjuhan01.zamestnanec Z
  WHERE P.ID_pozice = Z.ID_pozice
  ORDER BY POZICE;
      
-- necháme si vypsat plán na provedení dotazu nad materializovaným pohledem a vidíme že cena takového dotazu je mnohem nižší
EXPLAIN PLAN SET
  STATEMENT_ID 'my_explained_query4'
FOR
SELECT * FROM honzuv_pohled;

SELECT plan_table_output
  FROM table (dbms_xplan.display('plan_table','my_explained_query4','typical'));     
    
      
      
